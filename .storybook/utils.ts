import { Meta, Story } from '@storybook/vue/types-6-0'
import { kebabCase } from 'lodash'

type Components = Record<string, unknown>

/**
 * Generate the metada of the component
 */
export const meta = (components: Components): Meta => {
  const names = Object.keys(components)
  if (names.length !== 1) {
    throw new Error('Only 1 component is supported to build the metadata')
  }
  const componentName = names[0]
      , componentTitle = componentName.slice(1, names[0].length)

  return {
    title: `Components/${componentTitle}`,
    component: components[componentName]
  }
}

/**
 * Generate a template that can be used to build stories
 */
export const template = (components: Components, { template }: { template: string } = { template: null }): Story => {
  const names = Object.keys(components)
  if (names.length !== 1) {
    throw new Error('Only 1 component is supported to build the template')
  }
  const componentElement = kebabCase(names[0])

  if (!template) {
    template = `<${componentElement} v-bind='args' />`
  }

  // TODO: add actions
  return (args: Record<string, unknown>) => ({
    components,
    setup() {
      return { args }
    },
    template
  })
}
