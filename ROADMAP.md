# Roadmap

This is a rough description of the next changes that will be included in the project.

## [0.6.0]
 - [X] Organize components to be easier to access their unit tests and Storybook stories
 - [X] Tree component.
 - [ ] Share Storybook stories or data with unit tests
 - [ ] Share Storybook stories or data with e2e tests

## [0.7.0]
 - [ ] Pre-fetch view data
 - [ ] Transition between views

## [0.8.0]
 - [ ] Pre-fetch component data
 - [ ] Component loading status

## [0.9.0]
 - [ ] Vuex
 - [ ] Shorter aliases `@/x` to `@x`
 - [ ] Shorter alias for `tests/setup`

## [0.10.0]
 - [ ] Mocks, stubs, spies (Sinon)
 - [ ] i18n

## [0.11.0]
 - [ ] Code coverage for unit tests
 - [ ] GitLab CI configuration

## [0.12.0]
 - [ ] Validation

## [1.0.0]
 - [ ] Authentication
 - [ ] User roles / privileges
 - [ ] Login component

## [1.2.0]
 - [ ] Style linter for SASS
 - [ ] Basic Pug linter

## [1.3.0]
 - [ ] Pug linter that adheres to the recommended rules of Vue style guide

## [1.4.0]
 - [ ] JSON linter

## [1.5.0]
 - [ ] Markdown linter

## [1.6.0]
 - [ ] Documentation generator

## [1.7.0]
 - [ ] Optimize asset build

## [1.8.0]
 - [ ] Automate changelog management

## [2.0.0]
 - [ ] Vite for serving the app on development 

## [2.1.0]
 - [ ] Vite for Storybook

## [2.x.0]
 - [ ] Upgrade to Vue 2.7

## [3.0.0]
 - [ ] Vue 3

## [x.y.z] - Nice to have
 - [ ] SSR
 - [ ] YAML linter
