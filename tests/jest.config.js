module.exports = {
  rootDir: '../',
  collectCoverageFrom: ['src/**/*.{js,ts,vue}', '!src/**/*.d.ts'],
  moduleFileExtensions: ['js', 'json', 'ts', 'vue'],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1'
  },
  snapshotResolver: '<rootDir>/tests/unit/snapshotResolver.js',
  setupFilesAfterEnv: [
    '<rootDir>/tests/unit/context-as-describe.js',
    '<rootDir>/tests/unit/jest-chai.js',
    '<rootDir>/tests/unit/sinon.js'
  ],
  testMatch: [
    '<rootDir>/src/**/*.spec.{js,ts}',
    '<rootDir>/tests/unit/**/*.spec.{js,ts}'
  ],
  transform: {
    '.*\\.vue$': '@vue/vue3-jest',
    '.*\\.ts$': 'ts-jest',
    '^.+\\.(c|sa)ss$': '<rootDir>/tests/unit/cssTransform.js'
  },
  transformIgnorePatterns: ['/node_modules/(?!oruga)'],
  testEnvironment: 'jsdom',
  watchPlugins: [
    'jest-watch-typeahead/filename',
    'jest-watch-typeahead/testname'
  ],

  // Stop after 10 failed tests
  bail: 10,
  globals: {
    'ts-jest': {
      tsconfig: './tests/tsconfig.json'
    },
    'vue-jest': {
      tsConfig: './tests/tsconfig.json',
      experimentalCSSCompile: false,
      pug: {
        // To not fail when using `v-slot:header` (`v-slot:header=""` could be used too)
        doctype: 'html'
      }
    }
  }
}
