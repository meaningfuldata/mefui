/// <reference types="chai" />

interface JestAssertions {
  toMatchSnapshot(snapshotName?: string): any
}

interface Assertions extends Chai.Assertion, JestAssertions {}

interface Chai.Assertion {
  // Used by chai-each
  each: any
}

interface Expect extends Chai.ExpectStatic {
  (val: any, message?: string): Assertions
}

declare const expect: Expect
