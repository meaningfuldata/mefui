const chai = require('chai'),
  chaiEach = require('chai-each')

chai.use(chaiEach)

// Make sure Chai and Jest `.not` play nice together
const originalNot = Object.getOwnPropertyDescriptor(
  chai.Assertion.prototype,
  'not'
).get

Object.defineProperty(chai.Assertion.prototype, 'not', {
  get() {
    Object.assign(this, this.assignedNot)
    return originalNot.apply(this)
  },
  set(newNot) {
    this.assignedNot = newNot
    return newNot
  }
})

// Combine both Chai and Jest matchers on expect
const jestExpect = global.expect

global.expect = actual => {
  const jestMatchers = jestExpect(actual)
  const chaiMatchers = chai.expect(actual)

  // Add middleware to Chai matchers to increment Jest assertions made
  const { assertionsMade } = jestExpect.getState()
  Object.defineProperty(chaiMatchers, 'to', {
    get() {
      jestExpect.setState({ assertionsMade: assertionsMade + 1 })
      return chai.expect(actual)
    }
  })

  const combinedMatchers = Object.assign(chaiMatchers, jestMatchers)
  return combinedMatchers
}

Object.keys(jestExpect).forEach(key => (global.expect[key] = jestExpect[key]))
