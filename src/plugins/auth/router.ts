// Returns Vue Router beforeEach handler, to prevent auth users to get login views
// and unauth users  to get auth views
export const getBeforeEach = (
  isUserLoggedIn: () => boolean,
  unloggedView: string,
  loggedView: string
): ((to: any, from: any, next: any) => void) => {
  return (to, from, next) => {
    if (to.matched.some((record: any) => record.meta.requiresAuth)) {
      if (!isUserLoggedIn()) {
        next({ name: unloggedView })
      } else {
        next()
      }
    } else if (
      to.matched.some((record: any) => record.meta.hideForAuth) &&
      isUserLoggedIn()
    ) {
      next({ name: loggedView })
    } else next()
  }
}
