import { meta, template } from '@.storybook/utils'
import { SIcon } from './'

export default meta({ SIcon })

const Template = template({ SIcon })

export const DefaultPack = Template.bind({})
DefaultPack.args = {
  icon: 'map'
}

export const OtherPack = Template.bind({})
OtherPack.args = {
  icon: 'map',
  pack: 'far'
}
