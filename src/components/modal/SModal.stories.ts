import { meta } from '@.storybook/utils'
import { actions } from '@storybook/addon-actions'
import { SModal } from './'
import { SIcon } from '@/components/'
import { h, ref } from 'vue'

export default meta({ SModal })

//TODO Complete storybook being able to close and open modal with buttons

const Template = args => ({
  components: { SModal, SIcon },
  setup() {
    return { args }
  },
  template: `
      <button @click='$refs.custom_modal.open()'>Open modal</button>
      <s-modal v-bind="args" ref="custom_modal">
        <template #header>
           <header class="modal-card-head">
              <h1 class="modal-card-title">Example Title</h1>
              <SIcon :icon='{ name: "times", isCircled: true }' class='is-clickable' @click='$refs.custom_modal.close()'/>
            </header>
        </template>
        <template #body>
          <div v-if="args.body" v-html="args.body" class="modal-card-body"/>
        </template>
        <template #footer>
          <footer v-if="args.footer" v-html="args.footer" class="modal-card-foot"/>
        </template>
      </s-modal>
    `
})

export const CustomModal = Template.bind({})
CustomModal.args = {
  width: '500',
  body: `<p>Contrary to popular belief, Lorem Ipsum is not simply random text.
    It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.
    Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the
    more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of
     the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections
      1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero,
       written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.
        The first line of Lorem Ipsum, "</p>`,
  footer: '<a href="https://storybook.js.org/">Built with Storybook</a>'
}
