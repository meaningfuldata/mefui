import type { NamedRoute } from '@/types/'
import type { Icon } from '@/components/'

export interface BarItem {
  title: string
  icon: Icon
  // TODO: route or event
  route?: NamedRoute
  event?: string
  isActive?: boolean
}
