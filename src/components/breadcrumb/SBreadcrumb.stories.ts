import { meta, template } from '@.storybook/utils'
import { SBreadcrumb, BreadcrumbItem } from './'

export default meta({ SBreadcrumb })

const Template = template({ SBreadcrumb })

const items: Array<BreadcrumbItem> = [
  { title: 'Root', icon: 'battery-empty', route: { name: 'root' } },
  { title: 'Parent', icon: 'battery-quarter', route: { name: 'home' } },
  { title: 'Children', icon: 'battery-half', route: { name: 'settings' } },
  {
    title: 'GrandChildren',
    icon: 'battery-full',
    route: { name: 'tab', params: { id: 1 } }
  }
]

export const Empty = Template.bind({})
Empty.args = {
  items: []
}

export const OneItem = Template.bind({})
OneItem.args = {
  items: [items[0]]
}

export const SomeItems = Template.bind({})
SomeItems.args = {
  items
}
