// import { SEmailField } from './'
// import BInput from 'buefy/src/components/input/Input.vue'
// import BField from 'buefy/src/components/field/Field.vue'
//
// describe('SEmailField.vue', () => {
//   let wrapper: any
//
//   beforeEach(() => {
//     wrapper = localMount(SEmailField, {
//       propsData: { value: '' }
//     })
//   })
//
//   it('renders correctly', () => {
//     expect(wrapper.html()).toMatchSnapshot()
//   })
//
//   it('without props', () => {
//     expect(wrapper.findComponent(BField).props('label')).to.be.eql('Email')
//   })
//
//   it('passing a label', async () => {
//     await wrapper.setProps({ label: 'Repeat Email' })
//     expect(wrapper.findComponent(BField).props('label')).to.be.eql(
//       'Repeat Email'
//     )
//   })
//
//   it('has not counter', () => {
//     expect(wrapper.findComponent(BInput).props('hasCounter')).to.be.false
//   })
// })
