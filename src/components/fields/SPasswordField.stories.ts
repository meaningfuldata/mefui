import { meta, template } from '@.storybook/utils'
import { SPasswordField } from './'

export default meta({ SPasswordField })

const Template = template({ SPasswordField })

export const Default = Template.bind({})
Default.args = {}
