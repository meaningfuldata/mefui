import { meta, template } from '@.storybook/utils'
import { SSearchField } from './'
import { action } from '@storybook/addon-actions'

export default meta({ SSearchField })

const Template = args => ({
  components: { SSearchField },
  setup() {
    return { args }
  },
  template: `
    <SSearchField v-bind="args" v-on:update:modelValue="searchInput">
    </SSearchField>
  `,
  methods: {
    searchInput: (val: string) => action('search-input')(val)
  }
})
export const WithLabel = Template.bind({})
WithLabel.args = {
  icon: 'search',
  inputConfig: {
    placeholder: 'Search placeholder...'
  },
  labelConfig: {
    addons: false
  }
}

export const WithoutLabel = Template.bind({})
WithoutLabel.args = {
  icon: 'search',
  inputConfig: {
    placeholder: 'Another search placeholder...'
  },
  hideLabel: true
}
