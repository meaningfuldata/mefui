import { meta, template } from '@.storybook/utils'
import { SSwitch } from './'

export default meta({ SSwitch })

const Template = template({ SSwitch })

export const Simple = Template.bind({})
Simple.args = {
  variant: 'danger',
  rounded: true
}
Simple.argTypes = {
  variant: {
    options: [null, 'success', 'danger', 'info', 'warning'],
    control: 'select'
  }
}
