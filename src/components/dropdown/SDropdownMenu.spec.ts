// import { SHeaderDropdown } from './'
// import BMenuItem from 'buefy/src/components/menu/MenuItem.vue'
// import BMenuList from 'buefy/src/components/menu/MenuList.vue'
//
// const menu = [
//   {
//     label: 'Example 1',
//     items: [
//       {
//         label: '1',
//         route: '/'
//       }
//     ]
//   },
//   {
//     label: 'Example 2',
//     items: [
//       {
//         label: '2',
//         route: '/'
//       },
//       { label: '3', route: '/' }
//     ]
//   },
//   {
//     label: 'Example 3',
//     items: [
//       {
//         label: '4',
//         route: '/'
//       }
//     ]
//   }
// ]
//
// describe('SHeaderDropdown.vue', () => {
//   const label = 'Example'
//   const icon = 'cog'
//   let wrapper: any
//
//   it('requires an icon or label', () => {
//     expect(() => {
//       localMount(SHeaderDropdown, { propsData: { menu } })
//     }).to.throw('SHeaderDropdown must have at least a label or icon')
//
//     expect(() => {
//       localMount(SHeaderDropdown, { propsData: { icon, menu } })
//     }).to.not.throw()
//
//     expect(() => {
//       localMount(SHeaderDropdown, { propsData: { label, menu } })
//     }).to.not.throw()
//
//     expect(() => {
//       localMount(SHeaderDropdown, { propsData: { label, icon, menu } })
//     }).to.not.throw()
//   })
//
//   describe('when passing a menu (`DropdownMenu`)', () => {
//     beforeEach(() => {
//       wrapper = localMount(SHeaderDropdown, {
//         propsData: { icon, menu }
//       })
//     })
//
//     it('generates the menu list', () => {
//       // NOTE: `BMenuList` renders 2 components when label is used
//       expect(wrapper.findAllComponents(BMenuList)).to.have.lengthOf(3 * 2)
//     })
//
//     it('generates the menu items', () => {
//       expect(wrapper.findAllComponents(BMenuItem)).to.have.lengthOf(4)
//     })
//   })
// })
