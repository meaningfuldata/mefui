import { meta, template } from '@.storybook/utils'
import { SList, SDropdownMenu } from '../'
import type { DropdownMenu, ListItem } from '../'

export default meta({ SList })

const Template = template({ SList })
const CustomTemplate = template(
  { SList },
  {
    template: `<s-list v-bind="args">
    <template #item-label='{ item }'>
      <div :style='{ display: "flex", justifyContent: "space-between" }'>
        <div>Custom label for <b>{{ item.label }}</b></div>
        <div>Additional: <b>{{ item.icon.name || item.icon }}</b></div>
      </div>
    </template>
  </s-list>`
  }
)
const CollapsedContentTemplate = template(
  { SList },
  {
    template: `<s-list v-bind="args">
    <template #item-content='{ item }'>
      Content for <b>{{ item.label }}</b>
    </template>
  </s-list>`
  }
)
const MenuTemplate = (args: Record<string, unknown>) => ({
  components: {
    SList,
    SDropdownMenu
  },
  setup() {
    return { args }
  },
  template: `<s-list :label='args.label' :items='args.items' is-collapsible>
    <template #title='{ label }'>
      <div style='display: flex; justify-content: space-between;'>
        <h3>{{ label }}</h3>
        <s-dropdown-menu 
          :icon='{ name: "ellipsis-v", isCircled: true }'
          :menu='args.dropdownMenu'
          @click='$event.stopPropagation()'
        />
      </div>
    </template>
    <template #item-label='{ item }'>
      <div style='display: flex; justify-content: space-between;'>
        <div>{{ item.label }}</div>
        <s-dropdown-menu 
          :icon='{ name: "ellipsis-v", isCircled: true }'
          :menu='args.dropdownMenu'
        />
      </div>
    </template>
  </s-list>`
})

const items: ListItem[] = [
  {
    label: 'Item 1',
    icon: {
      name: 'table',
      title: 'Table'
    }
  },
  {
    label: 'Item 2',
    icon: 'cog',
    isExpanded: true
  }
]

export const Example = Template.bind({})
Example.args = {
  label: 'Fruits',
  items
}

export const Custom = CustomTemplate.bind({})
Custom.args = {
  label: 'Fruits',
  items
}

export const CollapsedContent = CollapsedContentTemplate.bind({})
CollapsedContent.args = {
  label: 'Fruits',
  items: [items[0], { ...items[1], isCollapsible: true }],
  isCollapsible: true
}

const dropdownMenu: DropdownMenu = [
  {
    title: 'Menu',
    items: [{ title: 'Action X' }, { title: 'Action Y' }]
  }
]

export const Menu = MenuTemplate.bind({})
Menu.args = {
  label: 'Menu example',
  items,
  dropdownMenu
}
