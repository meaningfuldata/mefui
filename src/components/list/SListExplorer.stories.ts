import { meta, template } from '@.storybook/utils'
import { SListExplorer, SDropdownMenu } from '../'
import type { DropdownMenu, List } from '../'

export default meta({ SListExplorer })

const Template = template({ SListExplorer })
const CustomTemplate = template(
  { SListExplorer },
  {
    template: `<s-list-explorer v-bind="args">
    <template #item-label='{ item }'>
      Custom label for <b>{{ item.label }}</b>
    </template>
  </s-list-explorer>`
  }
)
const CollapsedContentTemplate = template(
  { SListExplorer },
  {
    template: `<s-list-explorer v-bind="args">
    <template #item-content='{ item }'>
      Content for <b>{{ item.label }}</b>
    </template>
  </s-list-explorer>`
  }
)
const MenuTemplate = (args: Record<string, unknown>) => ({
  components: {
    SListExplorer,
    SDropdownMenu
  },
  setup() {
    return { args }
  },
  template: `<s-list-explorer :lists='args.lists'>
    <template #title='{ list }'>
      <div style='display: flex; justify-content: space-between;'>
        <h3>{{ list.label }}</h3>
        <s-dropdown-menu 
          :icon='{ name: "ellipsis-v", isCircled: true }'
          :menu='list.menu'
        />
      </div>
    </template>
    <template #item-label='{ item }'>
      <div style='display: flex; justify-content: space-between;'>
        {{ item.label }}
        <s-dropdown-menu 
          :icon='{ name: "ellipsis-v", isCircled: true }'
          :menu='item.menu'
        />
      </div>
    </template>
    <template #item-content='{ item }'>
      Content for <b>{{ item.label }}</b>
    </template>
  </s-list-explorer>`
})

const lists: List[] = [
  {
    // label: 'List A',
    label: 'Input',
    items: [
      { label: 'Point A.1', icon: 'table', isExpanded: true },
      { label: 'Point A.2', icon: 'table' },
      { label: 'Point A.3', icon: 'table' }
    ]
  },
  {
    // label: 'List B',
    label: 'Intermediate',
    items: [
      { label: 'Point B.1', icon: 'table' },
      { label: 'Point B.2', icon: 'table' }
    ]
  },
  {
    // label: 'List C',
    label: 'Output',
    items: [
      { label: 'Point C.1', icon: 'table' },
      { label: 'Point C.2', icon: 'table', isExpanded: true },
      { label: 'Point C.3', icon: 'table' },
      { label: 'Point C.4', icon: 'table' }
    ]
  }
]

export const Empty = Template.bind({})
Empty.args = {
  lists: []
}

export const Single = Template.bind({})
Single.args = {
  lists: [lists[0]]
}

export const Multiple = Template.bind({})
Multiple.args = {
  lists
}

export const SearchDisabled = Template.bind({})
SearchDisabled.args = {
  isSearchable: false,
  lists
}

export const Custom = CustomTemplate.bind({})
Custom.args = {
  lists
}

export const CollapsedContent = CollapsedContentTemplate.bind({})
CollapsedContent.args = {
  lists: lists.map(list => {
    return {
      ...list,
      isCollapsible: true,
      items: list.items.map(item => ({
        ...item,
        isCollapsible: true
      }))
    }
  })
}

const dropdownMenu: DropdownMenu = [
  {
    title: 'Menu',
    items: [{ title: 'Action X' }, { title: 'Action Y' }]
  }
]

export const Menu = MenuTemplate.bind({})
Menu.args = {
  lists: lists.map(list => {
    return {
      ...list,
      menu: dropdownMenu,
      items: list.items.map(item => ({
        ...item,
        menu: dropdownMenu
      }))
    }
  })
}
