import type { ContexMenuEntry } from './types'

const contextMenuExample: ContextMenuEntry<string>[] = [
  {
    label: 'Option A',
    value: 'A'
  },
  {
    label: 'Option B',
    value: 'B'
  },
  {
    label: 'Option C',
    value: 'C',
    isDisabled: true
  }
]

const menuExample = [
  { title: 'Menu 1', upperRight: 'Extra info', downRight: 'Extra info' },
  { title: 'Menu 2', downLeft: 'Extra info', upperRight: 'More extra info' },
  {
    title: 'Menu 3',
    disabled: true,
    downRight: 'More extra info',
    downLeft: 'More extra info'
  }
]

export { contextMenuExample, menuExample }
