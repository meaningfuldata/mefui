// import BTable from 'buefy/src/components/table/Table.vue'
// import BTableColumn from 'buefy/src/components/table/TableColumn.vue'
// import BTooltip from 'buefy/src/components/tooltip/Tooltip.vue'
// import STable from './STable.vue'
// import { Column, Row } from './types'
//
// const columns: Array<Column> = [
//   { label: 'Example Column', field: 'example', tooltip: 'An example' },
//   { label: 'Other Column', field: 'other' }
// ]
// const _rows: Array<Row> = [
//   { example: 'Value 1', other: 'Other value 1' },
//   { example: 'Value 2' }
// ]
//
// const getTable = (wrapper: any): any => {
//   return wrapper.findComponent(BTable)
// }
//
// describe('STable.vue', () => {
//   let wrapper: any
//   let table: any
//
//   beforeEach(() => {
//     wrapper = localMount(STable, {
//       propsData: { columns, rows: [] }
//     })
//     table = getTable(wrapper)
//   })
//
//   it('renders correctly', () => {
//     expect(wrapper.html()).toMatchSnapshot()
//   })
//
//   it('requires columns', () => {
//     expect(() => {
//       localMount(STable, {
//         propsData: { columns: [], rows: [] }
//       })
//     }).to.throw('STable component must have columns')
//   })
//
//   it('has sticky headers', () => {
//     expect(table.props('stickyHeader')).to.be.true
//   })
//
//   it('has striped rows', () => {
//     expect(table.props('striped')).to.be.true
//   })
//
//   it('is hoverable', () => {
//     expect(table.props('hoverable')).to.be.true
//   })
//
//   it('is paginated', () => {
//     expect(table.props('paginated')).to.be.true
//   })
//
//   it('is scrollable', () => {
//     expect(table.props('scrollable')).to.be.true
//   })
//
//   describe('each column', () => {
//     let tableColumns: any
//
//     beforeEach(() => {
//       tableColumns = wrapper.findAllComponents(BTableColumn)
//     })
//
//     it('set its labels', () => {
//       expect(tableColumns.at(0).props('label')).to.include(columns[0].label)
//       expect(tableColumns.at(1).props('label')).to.include(columns[1].label)
//     })
//
//     it('is sortable', () => {
//       expect(tableColumns.at(0).props('sortable')).to.be.true
//       expect(tableColumns.at(1).props('sortable')).to.be.true
//     })
//
//     it('is searchable', () => {
//       expect(tableColumns.at(0).props('searchable')).to.be.true
//       expect(tableColumns.at(1).props('searchable')).to.be.true
//     })
//
//     xit('can use a tooltip', () => {
//       const tooltip = tableColumns.at(0).findComponent(BTooltip)
//       expect(tooltip.props('label')).to.include(columns[0].tooltip)
//
//       const noTooltip = tableColumns.at(1).findComponent(BTooltip)
//       expect(noTooltip.exists()).to.be.false
//     })
//   })
// })
