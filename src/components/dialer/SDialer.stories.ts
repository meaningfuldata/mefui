import { meta, template } from '@.storybook/utils'
import { SDialer } from './'
import { ref } from 'vue'

export default meta({ SDialer })

const Template = args => ({
  components: {
    SDialer
  },
  setup() {
    const value = ref(0)
    return { args, value }
  },
  template: `
    <SDialer v-bind="args" v-model='value' />
    `
})

export const Simple = Template.bind({})
Simple.args = {
  options: [
    {
      name: 'Name',
      value: 0
    },
    {
      name: 'Code',
      value: 1
    },
    {
      name: 'Name:Code',
      value: 2
    }
  ],
  title: 'Resource display mode'
}

export const Icons = Template.bind({})
Icons.args = {
  icon: 'user',
  options: [
    {
      name: 'Name',
      value: 0,
      icon: 'user'
    },
    {
      name: 'Code',
      value: 1,
      icon: 'cog'
    },
    {
      name: 'Name:Code',
      value: 2
    }
  ],
  title: 'Resource display mode'
}

export const CustomLabel = Template.bind({})

CustomLabel.args = {
  label: 'Test label',
  options: [
    {
      name: 'Name',
      value: 0,
      icon: 'user'
    },
    {
      name: 'Code',
      value: 1,
      icon: 'cog'
    },
    {
      name: 'Name:Code',
      value: 2
    }
  ],
  title: 'Resource display mode'
}
