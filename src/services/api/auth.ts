import { getConfig } from '@/config'
import type {
  Credentials,
  ChangePassword,
  ResetPassword,
  Email
} from '@/types/'
import client from './client'

const getUrl = (path: string) => getConfig(`SERVICES.API.AUTH.${path}`)

// TODO maybe this could be a get
export const csrf = (): Promise<boolean> =>
  handleResponse(client.post(getUrl('CSRF'), {}))

export const signIn = async (credentials: Credentials): Promise<boolean> => {
  await csrf()
  return handleResponse(client.post(getUrl('SESSION'), credentials))
}

export const isAuthenticated = (): Promise<boolean> =>
  handleResponse(client.get(getUrl('SESSION')))

export const signOut = (): Promise<boolean> =>
  handleResponse(client.delete(getUrl('SESSION')))

export const changePassword = (
  changePasswordData: ChangePassword
): Promise<boolean> =>
  handleResponse(client.patch(getUrl('PASSWORD_CHANGE'), changePasswordData))

export const resetPassword = (email: Email): Promise<boolean> =>
  handleResponse(client.post(getUrl('PASSWORD_RESET'), email))

export const setNewPassword = (setPasswordData: ResetPassword) =>
  handleResponse(client.patch(getUrl('PASSWORD_RESET'), setPasswordData))

const handleResponse = async (request: Promise<any>): Promise<boolean> => {
  try {
    await request
    return true
  } catch (_error) {
    return false
  }
}
