const projectName = 'Soil'

const account = {
  settings: 'Account settings'
}

const auth = {
  forgottenPassword: 'Forgotten password?',
  signIn: 'Sign In',
  signOut: 'Sign Out'
}

const routes = {
  home: 'Home'
}

// Messages used in components
const components = {
  Account: {
    PasswordChange: {
      home: routes.home,
      changePassword: 'Change password',
      currentPassword: 'Current Password:',
      newPassword: 'New Password:',
      repeatPassword: 'Repeat New Password:',
      saveChanges: 'Save Changes',
      statement:
        'To keep your account secure, your password must follow this rules:',
      rule1: 'At least 8 characters longs.',
      rule2: 'Should include characters, numbers and symbols.',
      rule3: 'Cannot include part of personal information (name, email...).',
      successMessage: 'Your password has been changed successfully.',
      link: 'Use your new password to {link}.',
      signIn: auth.signIn
    }
  },
  EmailField: {
    email: 'Email'
  },
  Form: {
    Submit: {
      submit: 'Submit'
    }
  },
  Navbar: {
    projectName,
    home: routes.home,
    settings: account.settings,
    signOut: auth.signOut
  },
  PasswordField: {
    password: 'Password'
  },
  SearchField: {
    search: 'Search'
  },
  Table: {
    previewMaximum: '(Preview maximum)',
    rowCount: '{count} rows',
    searchPlaceholder: 'Search...',
    selectPlaceholder: 'Select...'
  },
  Tabs: {
    loading: 'Loading...',
    menuOpen: 'Open tab menu',
    closeMultipleTabs: 'Close multiple tabs',
    closeOtherTab: 'Close other tabs',
    closeTab: 'Close tab',
    closeTabsToLeft: 'Close tabs to the left',
    closeTabsToRight: 'Close tabs to the right'
  },
  UsernameField: {
    username: 'Username'
  }
}

const layouts = {
  Authenticated: {}
}

const views = {
  '404': {
    error: 'Error occurred! Page could not be found'
  },
  Auth: {
    PasswordReset: {
      email: 'Email',
      error: 'Failed to send recovery email',
      forgottenPassword: auth.forgottenPassword,
      forgottenMessage:
        'No worries. Just enter the email address that you registered with below and we will send you a link to reset it.',
      submit: components.Form.Submit.submit,
      successMessage1:
        'Instructions for resetting your password has been sent to {email}.',
      successMessage2:
        'Please, ensure that the address is correct and check your spam folder if you do not receive it.'
    },
    SignIn: {
      forgottenPassword: auth.forgottenPassword,
      signIn: auth.signIn,
      signInTo: `Sign in to ${projectName}`,
      error: 'Error while trying to sign in'
    },
    SignOut: {
      confirmation: `You are signed out of ${projectName}.`,
      redirected: `You will be redirected now to {link}.`,
      signIn: auth.signIn.toLowerCase(),
      signedOut: `Signed out of ${projectName}`
    }
  }
}

const en = {
  S: {
    ...components,
    Layout: layouts,
    View: views
  }
}

export default en
