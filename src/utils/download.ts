// FIXME: fail without name
export const downloadFile = (blob: any, blobName: any) => {
  if (navigator.msSaveBlob) {
    // IE 10+
    navigator.msSaveBlob(blob, blobName)
  } else {
    const link = document.createElement('a')
    if (link.download !== undefined) {
      // feature detection
      // Browsers that support HTML5 download attribute
      const url = URL.createObjectURL(blob)
      link.setAttribute('href', url)
      link.setAttribute('download', blobName)
      link.style.visibility = 'hidden'
      document.body.appendChild(link)
      link.click()
      document.body.removeChild(link)
    }
  }
}
const convertArrayOfObjectsToCSV = (data: Array<any>) => {
  const columnDelimiter = ','
  const lineDelimiter = '\n'

  const header = Object.keys(data[0])
    .map((key: string) => {
      if (!key.startsWith('_')) return `"${key}"`
    })
    .join(columnDelimiter)

  const lines = data
    .map(
      row =>
        Object.values(row)
          .map(String) // convert every value to String
          .map(v => (v as any).replaceAll('"', '""')) // escape double colons
          .map(v => `"${v}"`) // quote it
          .join(columnDelimiter) // column delimiter
    )
    .join(lineDelimiter) // rows starting on new lines

  return header.concat(lineDelimiter, lines)
}

export const downloadJSON = (data: any, filename: string) => {
  const file = JSON.stringify(data, null, 2)
  const blob = new Blob([file], { type: 'text/plain' })
  downloadFile(blob, filename)
}

export const downloadImage = (image: any, filename: string) => {
  const link = document.createElement('a')

  link.setAttribute('href', image)
  link.setAttribute('download', filename)
  document.body.appendChild(link)
  link.click()
  document.body.removeChild(link)
}

export const downloadCSV = (data: Array<any>, filename: string) => {
  if (!(data && filename)) {
    throw new Error('Data and filename can not be empty')
  }

  const csv = convertArrayOfObjectsToCSV(data)
  const blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' })
  downloadFile(blob, filename)
}

export const readJsonFile = async (file: File) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()

    reader.onload = event => {
      let error: string | null = event.target!.error
        ? 'Error while loading the file'
        : null

      if (!error) {
        const content: any = event.target!.result
        if (content) {
          try {
            return resolve(JSON.parse(content))
          } catch (_error) {
            error = 'Error while parsing file as JSON'
          }
        } else {
          error = 'No file found'
        }
      }

      reject(new Error(error))
    }

    reader.readAsText(file)
  })
}
