import { get, set, merge } from 'lodash'

const defaultConfig = Object.freeze({
  PROJECT: {
    NAME: 'Soil',
    URL: 'https://gitlab.com/meaningfuldata/soil'
  },
  COMPONENTS: {
    SIcon: {
      defaultIconPack: 'fas'
    }
  },
  SERVICES: {
    API: {
      BASE_URL: 'http://localhost/api/',
      AUTH: {
        CSRF: '/auth/csrf/',
        SESSION: '/auth/session/',
        PASSWORD_CHANGE: '/auth/password/change/',
        PASSWORD_RESET: '/auth/password/reset/'
      }
    }
  }
})

let config = {
  ...defaultConfig
}

/**
 * Get a value of the configuration
 */
const getConfig = (path: string) => get(config, path)

/**
 * Set a value of the configuration
 */
const setConfig = (path: string, value: any) => set(config, path, value)

/**
 * Combine configurations
 */
const mergeConfig = (newConfig: any) => {
  config = merge(config, newConfig)
}

export { getConfig, mergeConfig, setConfig }
