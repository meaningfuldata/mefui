// NOTE: some browsers do not support this event interface yet
// @see https://developer.mozilla.org/en-US/docs/Web/API/SubmitEvent
export type SubmitEvent = Event
